import java.time.LocalDate;

public class HistoryRecord {

    private Item item;
    private User user;
    private LocalDate lendAt;
    private LocalDate returnAt;

    public HistoryRecord(Item item, User user, LocalDate lendAt, LocalDate returnAt) {
        this.item = item;
        this.user = user;
        this.lendAt = lendAt;
        this.returnAt = returnAt;
    }

    public Item getItem() {
        return item;
    }

    public User getUser() {
        return user;
    }

    public LocalDate getLendAt() {
        return lendAt;
    }

    public LocalDate getReturnAt() {
        return returnAt;
    }
}

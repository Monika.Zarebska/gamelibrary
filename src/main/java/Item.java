import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Item implements Reporting {

    private UUID itemCode;
    private String name;
    private User lendBy;
    private LocalDate lendAt;
    private List<HistoryRecord> lendingHistory;

    public Item(UUID itemCode, String name) {
        this.itemCode = itemCode;
        this.name = name;
        this.lendingHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public UUID getItemCode() {
        return itemCode;
    }

    public User getLendBy() {
        return lendBy;
    }

    public void setLendBy(User lendBy) {
        this.lendBy = lendBy;
    }

    public LocalDate getLendAt() {
        return lendAt;
    }

    public void setLendAt(LocalDate lendAt) {
        this.lendAt = lendAt;
    }

    public List<HistoryRecord> getLendingHistory() {
        return lendingHistory;
    }

    @Override
    public String reportToJSON() {
        return """
                {"itemCode":"%s","name":"%s", "lendBy":"%s","lendAt":"%s"}
                """.formatted(itemCode, name, lendBy == null ? null : lendBy.getCardId(), lendAt);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return itemCode.equals(item.itemCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemCode);
    }
}

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ItemsAPI {

    //baza Itemów - jak dodam item to sie tu znajdzie
    Set<Item> items = new HashSet<>();


    public Set<Item> searchItems() {
        return items;
    }

    public Set<Item> searchItemsByName(String name) {
        Set<Item> result = new HashSet<>();
        for (Item item : items) {
            if (item.getName().equals(name))
                result.add(item);
        }
        return result;
    }

    public Item addItem(String name) {
        Item item = new Item(UUID.randomUUID(), name);
        this.items.add(item);
        return item;
    }

    public Item searchItemByCode(UUID itemCode) {
        for (Item item : items) {
            if (item.getItemCode().equals(itemCode)) {
                return item;
            }
        }
        throw new IllegalArgumentException("No such element in catalog.");
    }
}
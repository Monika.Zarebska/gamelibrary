import java.time.LocalDate;
import java.util.UUID;

public class LendingAPI {

    private final ItemsAPI itemsAPI;
    private final UsersAPI usersAPI;

    public LendingAPI(ItemsAPI itemsAPI, UsersAPI usersAPI) {
        this.itemsAPI = itemsAPI;
        this.usersAPI = usersAPI;
    }

    public User lendItem(String cardId, UUID itemCode) {
        User user = usersAPI.searchUserByCardId(cardId);
        Item item = itemsAPI.searchItemByCode(itemCode);
        user.getLentItems().add(item);
        item.setLendBy(user);
        item.setLendAt(LocalDate.now());
        return user;
    }

    public Item returnItem(UUID itemCode) {
        Item item = itemsAPI.searchItemByCode(itemCode);
        User user = item.getLendBy();
        HistoryRecord record = new HistoryRecord(item, user, item.getLendAt(), LocalDate.now());

        user.getLentItems().remove(item);
        user.getLendingHistory().add(record);

        item.setLendBy(null);
        item.setLendAt(null);
        item.getLendingHistory().add(record);

        return item;
    }
}

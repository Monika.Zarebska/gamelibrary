import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ReportingAPI {

    private final ItemsAPI itemsAPI;
    private final UsersAPI usersAPI;

    public ReportingAPI(ItemsAPI itemsAPI, UsersAPI usersAPI) {
        this.itemsAPI = itemsAPI;
        this.usersAPI = usersAPI;
    }

    public List<String> generateJSONs() {
        Set<Item> items = itemsAPI.searchItems();
        Set<User> users = usersAPI.searchUsers();
        List<Reporting> everything = new ArrayList<>();
        everything.addAll(items);
        everything.addAll(users);
        return everything.stream()
                .map(oneThing -> oneThing.reportToJSON())
                .toList();
    }
}

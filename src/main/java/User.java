import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User implements Reporting {

    private String cardId;
    private String firstName;
    private String lastName;
    private String pesel;
    private List<Item> lentItems;
    private List<HistoryRecord> lendingHistory;

    public User(String cardId, String firstName, String lastName, String pesel) {
        this.cardId = cardId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.lentItems = new ArrayList<>();
        this.lendingHistory = new ArrayList<>();
    }

    public User(String cardId, String firstName, String lastName, String pesel, List<Item> lentItems) {
        this.cardId = cardId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.lentItems = lentItems;
        this.lendingHistory = new ArrayList<>();
    }

    public String getCardId() {
        return cardId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public List<Item> getLentItems() {
        return lentItems;
    }

    public List<HistoryRecord> getLendingHistory() {
        return lendingHistory;
    }

    @Override
    public String reportToJSON() {
        return """
                {"cardId":"%s", "firstName":"%s", "lastName":"%s", "pesel":"%s", "lentItems":"%s"} 
                """.formatted(cardId, firstName, lastName, pesel, lentItems.stream().map(Item::getItemCode).toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return cardId.equals(user.cardId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId);
    }
}

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class UsersAPI {

    Set<User> users = new HashSet<>();


    public User registerUser(String firstName, String lastName, String pesel) {
        for (User user : users) {
            if (pesel.equals(user.getPesel())) {
                throw new IllegalArgumentException("User pesel already registered");
            }
        }
        User user = new User(UUID.randomUUID().toString(), firstName, lastName, pesel);
        users.add(user);
        return user;
    }

    public User searchUserByCardId(String cardId) {
        for (User user : users) {
            if (cardId.equals(user.getCardId())) {
                return user;
            }
        }
        throw new IllegalArgumentException("User does not exist.");
    }

    public Set<User> searchUsers() {
        return users;
    }
}
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FunctionalTest {

    private static ItemsAPI itemsAPI;
    private static LendingAPI lendingAPI;
    private static UsersAPI usersAPI;
    private static ReportingAPI reportingAPI;
    private static Item item;
    private static User user;

    @BeforeAll
    static void beforeTests() {
        itemsAPI = new ItemsAPI();
        usersAPI = new UsersAPI();
        lendingAPI = new LendingAPI(itemsAPI, usersAPI);
        reportingAPI = new ReportingAPI(itemsAPI,usersAPI);

        item = itemsAPI.addItem("Splendor");
        user = usersAPI.registerUser("Monika", "Lewynsky", "99052861880");
    }

    @Test
    void lendingItemToNotRegisteredUserFailed() {
        UUID itemCode = item.getItemCode();

        assertThatIllegalArgumentException().isThrownBy(()
                -> lendingAPI.lendItem("xyz", itemCode));
    }

    @Test
    void lendingItemWhichNotExist() {
        String cardId = user.getCardId();
        assertThatIllegalArgumentException().isThrownBy(()
                -> lendingAPI.lendItem(cardId, UUID.randomUUID()));
    }

    @Test
    @Order(1)
    void lendingItemToRegisteredUser() {
        //Given
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();

        //When
        User modifiedUser = lendingAPI.lendItem(cardId, itemCode);

        //Then
        assertThat(modifiedUser.getLentItems()).hasSize(1).allMatch(lentItem -> lentItem.getName().equals("Splendor"));
        Item itemInSearch = itemsAPI.searchItemByCode(itemCode);
        assertThat(itemInSearch.getLendBy()).isEqualTo(modifiedUser);
        assertThat(itemInSearch.getLendAt()).isEqualTo(LocalDate.now());

        User userInSearch = usersAPI.searchUserByCardId(cardId);
        assertThat(userInSearch.getLentItems()).hasSize(1).allMatch(lentItem -> lentItem.getName().equals("Splendor"));
    }

    @Test
    @Order(2)
    void returnItemToLibrary() {
        //Given
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();

        //When
        Item returnedItem = lendingAPI.returnItem(itemCode);

        //Then
        assertThat(returnedItem.getLendBy()).isNull();
        Item itemInSearch = itemsAPI.searchItemByCode(itemCode);
        assertThat(itemInSearch.getLendBy()).isNull();
        assertThat(itemInSearch.getLendAt()).isNull();
        User userInSearch = usersAPI.searchUserByCardId(cardId);
        assertThat(userInSearch.getLentItems()).isEmpty();
    }

    @Test
    @Order(3)
    void itemHistory() {
        //Given
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();

        //When
        Item itemInSearch = itemsAPI.searchItemByCode(itemCode);

        //Then
        assertThat(itemInSearch.getLendingHistory())
                .hasSize(1)
                .allMatch(historyRecord -> historyRecord.getItem().getItemCode().equals(itemCode))
                .allMatch(historyRecord -> historyRecord.getUser().getCardId().equals(cardId))
                .allMatch(historyRecord -> historyRecord.getLendAt().equals(LocalDate.now()))
                .allMatch(historyRecord -> historyRecord.getReturnAt().equals(LocalDate.now()));
    }

    @Test
    @Order(4)
    void userHistory(){
        //Given
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();

        //When
        User userInSearch = usersAPI.searchUserByCardId(cardId);

        //Then
        assertThat(userInSearch.getLendingHistory())
                .hasSize(1)
                .allMatch(historyRecord -> historyRecord.getItem().getItemCode().equals(itemCode))
                .allMatch(historyRecord -> historyRecord.getUser().getCardId().equals(cardId))
                .allMatch(historyRecord -> historyRecord.getLendAt().equals(LocalDate.now()))
                .allMatch(historyRecord -> historyRecord.getReturnAt().equals(LocalDate.now()));
    }

    @Test
    @Order(5)
    void reportJSONs(){
        //When
        List<String> result = reportingAPI.generateJSONs();

        //Then
        assertThat(result).hasSize(2)
                .anyMatch(line -> line.contains("\"firstName\":\"Monika\""))
                .anyMatch(line -> line.contains("\"lastName\":\"Lewynsky\""))
                .anyMatch(line -> line.contains("\"pesel\":\"99052861880\""))
                .anyMatch(line -> line.contains("\"cardId\":\"%s\"".formatted(user.getCardId())))
                .anyMatch(line -> line.contains("\"name\":\"Splendor\""))
                .anyMatch(line -> line.contains("\"lendBy\":\"null\""))
                .anyMatch(line -> line.contains("\"lendAt\":\"null\""))
                .anyMatch(line -> line.contains("\"itemCode\":\"%s\"".formatted(item.getItemCode())));
    }
}

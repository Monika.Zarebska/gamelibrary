import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ItemsAPITest {

    private ItemsAPI itemsAPI;

    @BeforeEach
    void setUp() {
        itemsAPI = new ItemsAPI();
    }

    @Test
    void searchOnEmptyLibraryReturnsNoResult() {
        Set<Item> items = itemsAPI.searchItems();
        assertThat(items).isEmpty();
    }

    @Test
    void searchItemsByName() {
        itemsAPI.addItem("Splendor");
        itemsAPI.addItem("Go");
        Set<Item> items = itemsAPI.searchItemsByName("Splendor");
        assertThat(items).allMatch(item -> item.getName().equals("Splendor"));
    }

    @Test
    void addItemToCatalog() {
        Item item = itemsAPI.addItem("Splendor");
        assertThat(item.getName()).isEqualTo("Splendor");
        assertThat(item.getItemCode()).isNotNull();
    }

    @Test
    void addTwoItemsForTheSameGameToCatalog() {
        Item item1 = itemsAPI.addItem("Splendor");
        Item item2 = itemsAPI.addItem("Splendor");
        assertThat(item2.getName()).isEqualTo(item1.getName());
        assertThat(item2.getItemCode()).isNotEqualTo(item1.getItemCode());
        Set<Item> items = itemsAPI.searchItemsByName("Splendor");
        assertThat(items).allMatch(item -> item.getName().equals("Splendor"));
        assertThat(items.size()).isEqualTo(2);
    }
}
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class LendingAPITest {

    private LendingAPI lendingAPI;
    @Mock
    private ItemsAPI itemsAPI;
    @Mock
    private UsersAPI usersAPI;

    @BeforeEach
    void setUp() {
        lendingAPI = new LendingAPI(itemsAPI, usersAPI);
    }

    @Test
    void lendingItemToRegisteredUser() {
        //Given
        User user = new User(UUID.randomUUID().toString(), "Monika", "Lewynsky", "99052861880");
        Item item = new Item(UUID.randomUUID(), "Splendor");
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();
        given(itemsAPI.searchItemByCode(itemCode)).willReturn(item);
        given(usersAPI.searchUserByCardId(cardId)).willReturn(user);

        //When
        User modifiedUser = lendingAPI.lendItem(cardId, itemCode);

        //Then
        assertThat(modifiedUser.getLentItems())
                .hasSize(1)
                .allMatch(lentItem -> lentItem.getName().equals("Splendor"))
                .allMatch(lentItem -> lentItem.getLendAt().equals(LocalDate.now()));
    }

    @Test
    void returnItemToLibrary() {
        //Given
        Item item = new Item(UUID.randomUUID(), "Splendor");
        List<Item> listOfItems = new ArrayList<>();
        listOfItems.add(item);
        User user = new User(UUID.randomUUID().toString(), "Monika", "Lewynsky", "99052861880", listOfItems);
        item.setLendBy(user);
        LocalDate lendAt = LocalDate.now().minusDays(1);
        item.setLendAt(lendAt);
        UUID itemCode = item.getItemCode();
        String cardId = user.getCardId();
        given(itemsAPI.searchItemByCode(itemCode)).willReturn(item);

        //When
        Item returnItem = lendingAPI.returnItem(itemCode);

        //Then
        assertThat(returnItem.getLendBy()).isNull();
        assertThat(returnItem.getLendAt()).isNull();
        assertThat(returnItem.getLendingHistory())
                .hasSize(1)
                .allMatch(historyRecord -> historyRecord.getItem().getItemCode().equals(itemCode))
                .allMatch(historyRecord -> historyRecord.getUser().getCardId().equals(cardId))
                .allMatch(historyRecord -> historyRecord.getLendAt().equals(lendAt))
                .allMatch(historyRecord -> historyRecord.getReturnAt().equals(LocalDate.now()));
    }
}
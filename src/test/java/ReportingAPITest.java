import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ReportingAPITest {

    private ReportingAPI reportingAPI;
    @Mock
    private ItemsAPI itemsAPI;
    @Mock
    private UsersAPI usersAPI;

    @BeforeEach
    void setUp() {
        reportingAPI = new ReportingAPI(itemsAPI, usersAPI);
    }

    @Test
    void reportLentItemAsJSON() {
        //Given
        User user = new User(UUID.randomUUID().toString(), "Monika", "Lewynsky", "99052861880");
        Item item = new Item(UUID.randomUUID(), "Splendor");
        item.setLendBy(user);
        item.setLendAt(LocalDate.parse("2022-05-06"));
        given(itemsAPI.searchItems()).willReturn(Set.of(item));

        //When
        List<String> result = reportingAPI.generateJSONs();

        //Then
        assertThat(result).hasSize(1)
                .allMatch(line -> line.contains("\"name\":\"Splendor\""))
                .allMatch(line -> line.contains("\"lendBy\":\"%s\"".formatted(user.getCardId())))
                .allMatch(line -> line.contains("\"lendAt\":\"2022-05-06\""))
                .allMatch(line -> line.contains("\"itemCode\":\"%s\"".formatted(item.getItemCode())));
    }

    @Test
    void reportNotLentItemAsJSON() {
        //Given
        Item item = new Item(UUID.randomUUID(), "Splendor");
        given(itemsAPI.searchItems()).willReturn(Set.of(item));

        //When
        List<String> result = reportingAPI.generateJSONs();

        //Then
        assertThat(result).hasSize(1)
                .allMatch(line -> line.contains("\"name\":\"Splendor\""))
                .allMatch(line -> line.contains("\"lendBy\":\"null\""))
                .allMatch(line -> line.contains("\"lendAt\":\"null\""))
                .allMatch(line -> line.contains("\"itemCode\":\"%s\"".formatted(item.getItemCode())));
    }
    @Test
    void reportUserWithNoItem() {
        //Given
        User user = new User(UUID.randomUUID().toString(),"Monika", "Lewynsky", "99052861880");
        given(usersAPI.searchUsers()).willReturn(Set.of(user));

        //When
        List<String> result = reportingAPI.generateJSONs();

        //Then
        assertThat(result).hasSize(1)
                .allMatch(line -> line.contains("\"firstName\":\"Monika\""))
                .allMatch(line -> line.contains("\"lastName\":\"Lewynsky\""))
                .allMatch(line -> line.contains("\"pesel\":\"99052861880\""))
                .allMatch(line -> line.contains("\"cardId\":\"%s\"".formatted(user.getCardId())));
    }
}
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class UsersAPITest {

    private UsersAPI usersAPI;

    @BeforeEach
    void setUp() {
        usersAPI = new UsersAPI();
    }

    @Test
    void registerUser() {
        User user = usersAPI.registerUser("Monika", "Lewynsky", "99052861880");
        assertThat(user.getCardId()).isNotEmpty();
        assertThat(user.getFirstName()).isEqualTo("Monika");
        assertThat(user.getLastName()).isEqualTo("Lewynsky");
        assertThat(user.getPesel()).isEqualTo("99052861880");
    }

    @Test
    void registerAlreadyRegisteredUserShouldFail() {
        usersAPI.registerUser("Monika", "Lewynsky", "99052861880");
        assertThatIllegalArgumentException().isThrownBy(()
                -> usersAPI.registerUser("Monika", "Lewynsky", "99052861880"));
    }

    @Test
    void searchUserByCardId() {
        User user = usersAPI.registerUser("Monika", "Lewynsky", "99052861880");
        User userSearched = usersAPI.searchUserByCardId(user.getCardId());
        assertThat(userSearched.getFirstName()).isEqualTo("Monika");
        assertThat(userSearched.getLastName()).isEqualTo("Lewynsky");
        assertThat(userSearched.getPesel()).isEqualTo("99052861880");
    }

    @Test
    void searchUserByCardIdFail() {
        usersAPI.registerUser("Monika", "Lewynsky", "99052861880");
        assertThatIllegalArgumentException().isThrownBy(()
                -> usersAPI.searchUserByCardId("xyx"));
    }
}